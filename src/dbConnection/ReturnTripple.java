package dbConnection;

public class ReturnTripple {

	String projects[];
	String experts[];
	String interests[];
	
	public ReturnTripple(String expert[], String project[], String interest[]) {
		this.experts = expert;
		this.projects = project;
		this.interests = interest;
	}
}
