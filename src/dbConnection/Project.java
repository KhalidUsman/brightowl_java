package dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Project {
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	final private String host = "brightowl.pro";
	final private String user = "betabrig_ml_kh";
	final private String passwd = "kSH#0!Ze^)n5[LU;o0";
	final private String dbName = "/betabrig_khalid?";
	
	final private String proHost = "brightowl.pro";
	final private String proUser = "betabrig_ml_kh";
	final private String proPasswd = "kSH#0!Ze^)n5[LU;o0";
	final private String proDbName = "/betabrig_brightowl?";
	
	public int _projectId;
	public int _crawlId;
	public String _projectName;
	public String _crawlText;
	
	public Project(int projectId) throws Exception {
		try {
		      // This will load the MySQL driver, each DB has its own driver
		      Class.forName("com.mysql.jdbc.Driver");
		      connect = DriverManager
		          .getConnection("jdbc:mysql://" + proHost + proDbName
		              + "user=" + proUser + "&password=" + proPasswd );
		      statement = connect.createStatement();
		      this.getProjectDetails(projectId);
		    } 
		    catch (Exception e) {
		      throw e;
		    }
	}
	
	/*
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}
	*/
	
	public void getProjectDetails(int projectId) {
		 try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM projects WHERE project_id = " + projectId);
			while (resultSet.next()) {
				this._projectId  = resultSet.getInt(1);
				this._projectName  = resultSet.getString(2);
				this._crawlId = resultSet.getInt("is_crawled");
				this._crawlText = resultSet.getString("crawled_job_detail");
				}
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String[] getProjectPersonalities() {
		
		String personalitites[];
		List<String> personality = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT p_title FROM personality WHERE p_id IN (SELECT personality_id FROM project_personality WHERE project_id = " + this._projectId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					personality.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		personalitites = personality.toArray(new String[personality.size()]);
		 return personalitites;
	}
	
	public String[] getProjectExpertise() {
		
		String projectise[];
		List<String> project = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT et_name FROM expertise_tags WHERE et_id IN (SELECT et_id FROM project_expertise WHERE project_id = " + this._projectId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					project.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		projectise = project.toArray(new String[project.size()]);
		 return projectise;
	}
	
	public String[] getProjectKnowledge() {
		
		String Knowledge[];
		List<String> know = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT know_title FROM knowledge WHERE know_id IN (SELECT knowledge_id FROM project_knowledge WHERE project_id = " + this._projectId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					know.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Knowledge = know.toArray(new String[know.size()]);
		 return Knowledge;
	}
	
	public String[] getProjectEducation() {
		
		String Education[];
		List<String> edu = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT education FROM educations WHERE education_id IN (SELECT education_id FROM project_educations WHERE project_id = " + this._projectId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					edu.add(resultSet.getString(1));
					}
				}
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Education = edu.toArray(new String[edu.size()]);
		 return Education;
	}
	
	public String[] getProjectContractType() {
			
			String Contrats[];
			List<String> contract = new ArrayList<String>();

			try {
				statement = connect.createStatement();
				resultSet = statement.executeQuery("SELECT project_type_name FROM project_type WHERE FIND_IN_SET( project_type_id, (SELECT project_type_id FROM projects WHERE project_id = " + this._projectId + "))");
				while (resultSet.next()) {
					if (resultSet.getString(1).length()>0) {
						contract.add(resultSet.getString(1));
						}
				    }
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
			Contrats = contract.toArray(new String[contract.size()]);
			 return Contrats;
	}
	
	public String[] getProjectLanguages() {
		
		String Languages[];
		List<String> lang = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT lang_name FROM languages WHERE lang_id IN (SELECT language_id FROM project_languages WHERE project_id = " + this._projectId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					lang.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Languages = lang.toArray(new String[lang.size()]);
		 return Languages;
	}
	
	public String[] getProjectFunctionalTitle() {
		
		String FuncTitles[];
		List<String> funcTitle = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT experience_title FROM experience WHERE experience_id IN (SELECT ftitle_id_original FROM project_ftitles WHERE project_id = " + this._projectId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					funcTitle.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		FuncTitles = funcTitle.toArray(new String[funcTitle.size()]);
		 return FuncTitles;
	}
	
	public String[] getProjectLocation() {
		
		String Location[];
		List<String> loc = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT postal_code, locality, province, region, country FROM project_location_detail WHERE project_id = " + this._projectId);
			int i;
			while (resultSet.next()) {
				String locStr = "";
				for (i=0; i<5;i++) {
					if (resultSet.getString(i+1).length()>0) {
						locStr = locStr + resultSet.getString(i+1) + " ";
					}
				}
				locStr = locStr.trim();
				if (locStr.length()>0) {
					loc.add(locStr);
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Location = loc.toArray(new String[loc.size()]);
		 return Location;
	}
	
	public String[] getProjectCoordinates() {
		
		String Coordinates[];
		List<String> coord = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT latitude, longitude FROM project_location_detail WHERE project_id = " + this._projectId);
			int i;
			while (resultSet.next()) {
				String coordStr = "";
				for (i=0; i<2;i++) {
					if (resultSet.getString(i+1).length()>0) {
						coordStr = coordStr + resultSet.getString(i+1) + " ";
					}
				}
				coordStr = coordStr.trim();
				if (coordStr.length()>0) {
					coord.add(coordStr);
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Coordinates = coord.toArray(new String[coord.size()]);
		 return Coordinates;
	}
	
	public void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
