package dbConnection;

import dbConnection.ReturnPair;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.SQLException;

public class Expert {

	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	final private String host = "brightowl.pro";
	final private String user = "betabrig_ml_kh";
	final private String passwd = "kSH#0!Ze^)n5[LU;o0";
	final private String dbName = "/betabrig_khalid?";
	
	final private String proHost = "brightowl.pro";
	final private String proUser = "betabrig_ml_kh";
	final private String proPasswd = "kSH#0!Ze^)n5[LU;o0";
	final private String proDbName = "/betabrig_brightowl?";
	
	ReturnPair returnPair;
	
	public int _expertId;
	public String _expertName;
	public String _expertLocation;
	public String _expertJobtitle;
	
	public Expert(int expertId) throws Exception {
		try {
		      // This will load the MySQL driver, each DB has its own driver
		      Class.forName("com.mysql.jdbc.Driver");
		      connect = DriverManager
		          .getConnection("jdbc:mysql://" + proHost + proDbName
		              + "user=" + proUser + "&password=" + proPasswd );
		      statement = connect.createStatement();
		      this.getExpertDetails(expertId);
		    } 
		    catch (Exception e) {
		      throw e;
		    }
	}
	
	/*
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}
	*/
	
	public void getExpertDetails(int expertId) {
		 try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM expert WHERE expert_id = " + expertId);
			while (resultSet.next()) {
				this._expertId  = resultSet.getInt(1);
				this._expertName  = resultSet.getString(2);
				this._expertLocation = resultSet.getString(4);
				this._expertJobtitle  = resultSet.getString(22);
				}
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String getExpertSummary() {
	
		String summary = "";
		 try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT expert_summary FROM expert WHERE expert_id = " + this._expertId);
			while(resultSet.next()) {
				summary = resultSet.getString(1).replaceAll("(\\<.*?\\>|&lt;.*?&gt;)", "");
			}
//			if (resultSet.getString(1).length()>0) {
//				
//			}
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		 return summary;
	}
	
	public String getExpertLinkedInSummary() {
		
		String linkedInSummary = "";
		 try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT expert_summary FROM expert WHERE expert_id = " + this._expertId);
			while(resultSet.next()) {
				linkedInSummary = resultSet.getString(1).replaceAll("(\\<.*?\\>|&lt;.*?&gt;)", "");
			}
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		 return linkedInSummary;
	}
	
	public String[] getExpertCertificates() {
		
		String certificates[];
		List<String> certs = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT expert_summary FROM expert WHERE expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					certs.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		certificates = certs.toArray(new String[certs.size()]);
		 return certificates;
	}
	
	public String[] getExpertTraining() {
		
		String training[];
		List<String> train = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT training_title FROM expert_training WHERE expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					train.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		training = train.toArray(new String[train.size()]);
		 return training;
	}
	
	public String[] getExpertPersonalities() {
		
		String personalitites[];
		List<String> personality = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT p_title FROM personality WHERE p_id IN (SELECT p_id FROM expert_personality WHERE expert_id = " + this._expertId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					personality.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		personalitites = personality.toArray(new String[personality.size()]);
		 return personalitites;
	}
	
	public String[] getExpertExpertise() {
		
		String expertise[];
		List<String> expert = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT et_name FROM expertise_tags WHERE et_id IN (SELECT et_id FROM expert_expertise WHERE expert_id = " + this._expertId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					expert.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		expertise = expert.toArray(new String[expert.size()]);
		 return expertise;
	}
	
	public String[] getExpertKnowledge() {
		
		String Knowledge[];
		List<String> know = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT know_title FROM knowledge WHERE know_id IN (SELECT know_id FROM expert_knowledge WHERE expert_id = " + this._expertId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					know.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Knowledge = know.toArray(new String[know.size()]);
		 return Knowledge;
	}
	
	public String[] getExpertEducation() {
		
		String Education[];
		List<String> edu = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT edu_degree, edu_field FROM expert_education WHERE expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					edu.add(resultSet.getString(1));
					}
				if (resultSet.getString(2).length()>0) {
					edu.add(resultSet.getString(2));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Education = edu.toArray(new String[edu.size()]);
		 return Education;
	}
	
	public String getExpertPublication() {
		
		String Publication = "";
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT pub_title FROM expert_publication WHERE expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					Publication = Publication + resultSet.getString(1) + " ";
					}
			    }
			Publication = Publication.trim();
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		 return Publication;
	}
	
	ReturnPair getExpertProfessionalExperience() {
		
		String jobTitles[];
		String jobSummary = "";
		
		List<String> jobTitle = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT p_e_title, p_e_start_date, p_e_end_date, p_e_summary FROM expert_professional_experience WHERE expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					jobTitle.add(resultSet.getString(1));
					}
				if (resultSet.getString(4).length()>0) {
					jobSummary = jobSummary + resultSet.getString(4) + " ";
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		jobTitles = jobTitle.toArray(new String[jobTitle.size()]);
		jobSummary = jobSummary.trim();
		this.returnPair = new ReturnPair(jobTitles, jobSummary);
		return this.returnPair;
	}
	
	public String[] getExpertContractType() {
			
			String Contrats[];
			List<String> contract = new ArrayList<String>();

			try {
				statement = connect.createStatement();
				resultSet = statement.executeQuery("SELECT work_regime FROM expert_contract_type WHERE expert_id = " + this._expertId);
				while (resultSet.next()) {
					if (resultSet.getString(1).length()>0) {
						contract.add(resultSet.getString(1));
						}
				    }
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
			Contrats = contract.toArray(new String[contract.size()]);
			 return Contrats;
	}
	
	public String[] getExpertLanguages() {
		
		String Languages[];
		List<String> lang = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT lang_name FROM languages WHERE lang_id IN (SELECT lang_id FROM expert_language WHERE expert_id = " + this._expertId + ")");
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					lang.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Languages = lang.toArray(new String[lang.size()]);
		 return Languages;
	}
	
	public String[] getExpertFunctionalTitle() {
		
		String FuncTitles[];
		List<String> funcTitle = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT function_title FROM expert WHERE expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					funcTitle.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		FuncTitles = funcTitle.toArray(new String[funcTitle.size()]);
		 return FuncTitles;
	}
	
	public String[] getExpertFuncTitleInterested() {
		
		String FuncTitlesInterested[];
		List<String> funcTitleInt = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			String sql = "SELECT experience_title FROM experience WHERE experience_id IN (SELECT experience_id FROM expert_wp_experience WHERE expert_id = " + this._expertId + " AND is_not_interested = '0')";
			resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					funcTitleInt.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		FuncTitlesInterested = funcTitleInt.toArray(new String[funcTitleInt.size()]);
		 return FuncTitlesInterested;
	}
	
	public String[] getExpertLocation() {
		
		String Location[];
		List<String> loc = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT postal_code, locality, province, region, country FROM expert_location_detail WHERE expert_id = " + this._expertId);
			int i;
			while (resultSet.next()) {
				String locStr = "";
				for (i=0; i<5;i++) {
					if (resultSet.getString(i+1).length()>0) {
						locStr = locStr + resultSet.getString(i+1) + " ";
					}
				}
				locStr = locStr.trim();
				if (locStr.length()>0) {
					loc.add(locStr);
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Location = loc.toArray(new String[loc.size()]);
		 return Location;
	}
	
	public String[] getExpertCoordinates() {
		
		String Coordinates[];
		List<String> coord = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT latitude, longitude FROM expert_location_detail WHERE expert_id = " + this._expertId);
			int i;
			while (resultSet.next()) {
				String coordStr = "";
				for (i=0; i<2;i++) {
					if (resultSet.getString(i+1).length()>0) {
						coordStr = coordStr + resultSet.getString(i+1) + " ";
					}
				}
				coordStr = coordStr.trim();
				if (coordStr.length()>0) {
					coord.add(coordStr);
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		Coordinates = coord.toArray(new String[coord.size()]);
		 return Coordinates;
	}
	
	public String[] getExpertLocationWP() {
		
		String LocationWP[];
		List<String> locWP = new ArrayList<String>();

		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("SELECT eld_location FROM expert_location_detail WHERE is_wp = 1 AND expert_id = " + this._expertId);
			while (resultSet.next()) {
				if (resultSet.getString(1).length()>0) {
					locWP.add(resultSet.getString(1));
					}
			    }
		} 
		 catch (SQLException e) {
			e.printStackTrace();
		}
		LocationWP = locWP.toArray(new String[locWP.size()]);
		 return LocationWP;
	}
	
	public void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
