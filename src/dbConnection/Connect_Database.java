package dbConnection;

import dbConnection.ReturnTripple;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.mysql.jdbc.DatabaseMetaData;

//serverDbName = 'betabrig_khalid'

public class Connect_Database {
	
	ReturnTripple returnTripple;
	
	public Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	final private String host = "brightowl.pro";
	final private String user = "betabrig_ml_kh";
	final private String passwd = "kSH#0!Ze^)n5[LU;o0";
	final private String dbName = "/betabrig_khalid?";
	
	final private String proHost = "brightowl.pro";
	final private String proUser = "betabrig_ml_kh";
	final private String proPasswd = "kSH#0!Ze^)n5[LU;o0";
	final private String proDbName = "/betabrig_brightowl?";
	
	public void readDataBase() throws Exception {
		    try {
		      // This will load the MySQL driver, each DB has its own driver
		      Class.forName("com.mysql.jdbc.Driver");
		      connect = DriverManager
		          .getConnection("jdbc:mysql://" + proHost + proDbName
		              + "user=" + proUser + "&password=" + proPasswd );
		      statement = connect.createStatement();
		      System.out.println("Connection Established Successfull and the DATABASE NAME IS:"
                      + connect.getMetaData().getDatabaseProductName());
		    } 
		    catch (Exception e) {
		      System.out.println("Unable to make connection with DB");
		      throw e;
		    }
		  }
	  
	  public void saveRecords(ArrayList<Map<String, Double>> matchingArr, String sql) throws Exception {
		    try {
		    	preparedStatement = connect.prepareStatement(sql);
		    	for (Map<String, Double> map : matchingArr) {
		    		for (Entry<String, Double> entry : map.entrySet()) {
		    			if (entry.getKey() == "expert_id") {
		    				preparedStatement.setInt(1, (int)((double)entry.getValue()));
		    		    }
		    		    else if (entry.getKey() == "project_id") {
		    			  preparedStatement.setInt(2, (int)((double)entry.getValue()));
		    		    }
		    		    else if (entry.getKey() == "ml_score") {
		    			  preparedStatement.setFloat(3, (float)((double)entry.getValue()));
		    			  preparedStatement.setFloat(4, (float)((double)entry.getValue()));
		    		  }
		    	  }
		    	preparedStatement.executeUpdate();
		      }
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public void saveRecord(Map<String, Double> map) throws Exception {
		    try {
		    	int projectId = 0;
		    	int expertId = 0;
		    	float ml_Score = 0;
		    	for (Entry<String, Double> entry : map.entrySet()) {
	    			if (entry.getKey() == "expert_id") {
	    				expertId = (int)((double)entry.getValue());
	    			}
	    		    else if (entry.getKey() == "project_id") {
	    		    	projectId = (int)((double)entry.getValue());
	    		    }
	    		    else if (entry.getKey() == "ml_score") {
	    		    	ml_Score = (float)((double)entry.getValue());
	    		  }
	    	  }

		    String sql = "INSERT INTO z_expert_project_complete_" + expertId + " (project_id, ml_score_kh, match_result, match_ml, ml_score_pm, created_at, updated_at) VALUES(" + projectId + "," + ml_Score + ",0 ,0 ,0, '0000-00-00 00:00:00', '0000-00-00 00:00:00') ON DUPLICATE KEY UPDATE ml_score_kh = " + ml_Score;
		    statement = connect.createStatement();
		    statement.executeUpdate(sql);
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public void InsertOrUpdateRecord(ArrayList<Map<String, Double>> matchingArr) throws Exception {
		    try {
		    	for (Map<String, Double> map : matchingArr) {
		    		for (Entry<String, Double> entry : map.entrySet()) {
		    			if (entry.getKey() == "expert_id") {
		    				DatabaseMetaData dbm = (DatabaseMetaData) connect.getMetaData();
		    				String tableName = "z_expert_project_complete_" + (int)((double)entry.getValue());
		    				ResultSet tables = dbm.getTables(null, null, tableName, null);
		    				if (tables.next()) {
		    					this.saveRecord(map);
		    				}
		    				else {
		    					String createSql = "CREATE TABLE " + tableName +
		    			                   "(project_id int(11) NOT NULL ," +
		    			                   " match_result float(8,2) NOT NULL DEFAULT '0'," + 
		    			                   " match_ml float(8,2) NOT NULL DEFAULT '0'," + 
		    			                   " ml_score_pm float(8,2) NOT NULL DEFAULT '0'," + 
		    			                   " ml_score_kh float(8,2) NOT NULL DEFAULT '0'," +
		    			                   " created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'," + 
		    			                   " updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'," +
		    			                   " PRIMARY KEY ( project_id ))";
		    					Statement st = connect.createStatement();
		    					st.executeUpdate(createSql);
		    					this.saveRecord(map);
		    				}
		    		    }
		    	  }
		      }
		    	System.out.println("Records Saved!");
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public void updateRecords(String sql) throws Exception {
		    try {
		    	Statement st = connect.createStatement();
				st.executeUpdate(sql);
				//preparedStatement = connect.prepareStatement(sql);
				//preparedStatement.executeUpdate();
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public void deleteRecords(ArrayList<Map<String, Double>> matchingArr, String sql) throws Exception {
		  try {
		      preparedStatement = connect.prepareStatement(sql);
		      for (Map<String, Double> map : matchingArr) {
		    	  for (Entry<String, Double> entry : map.entrySet()) {
		    		  if (entry.getKey() == "expert_id") {
		    			  preparedStatement.setInt(1, (int)((double)entry.getValue()));
		    		  }
		    		  else if (entry.getKey() == "project_id") {
		    			  preparedStatement.setInt(2, (int)((double)entry.getValue()));
		    		  }
		    		  else if (entry.getKey() == "ml_score") {
		    			  preparedStatement.setFloat(3, (float)((double)entry.getValue()));
		    			  preparedStatement.setFloat(4, (float)((double)entry.getValue()));
		    		  }
		    	  }
		      }
		      preparedStatement.executeUpdate();
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public ResultSet getAllExperts() throws Exception {
		    try {
		    	statement = connect.createStatement();
		    	resultSet = statement.executeQuery("SELECT expert_id FROM expert");
		    	return resultSet;
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public ResultSet getAllProjects() throws Exception {
		    try {
		    	statement = connect.createStatement();
		    	resultSet = statement.executeQuery("SELECT project_id,crawled_job_detail,is_crawled FROM projects");
		    	return resultSet;
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public ResultSet getExperts() throws Exception {
		    try {
		    	statement = connect.createStatement();
		    	resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE profile_complete_percent >= 60");
		    	return resultSet;
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public ResultSet getProjectsWithId() throws Exception {
		    try {
		    	statement = connect.createStatement();
		    	resultSet = statement.executeQuery("SELECT project_id FROM projects WHERE k_ml_update = 1 ORDER BY updated_at DESC LIMIT 1");
		    	//resultSet = statement.executeQuery("SELECT project_id,crawled_job_detail,is_crawled FROM projects");
		    	return resultSet;
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public ResultSet getProjects() throws Exception {
		    try {
		    	statement = connect.createStatement();
		    	resultSet = statement.executeQuery("SELECT project_id FROM projects");
		    	return resultSet;
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  public ResultSet getExpertsWithId() throws Exception {
		    try {
		    	statement = connect.createStatement();
		    	resultSet = statement.executeQuery("SELECT expert_id FROM expert WHERE profile_complete_percent >= 60 AND k_ml_update = 1 ORDER BY updated_at DESC LIMIT 1");
		    	return resultSet;
		    } 
		    catch (Exception e) {
		      throw e;
		    }
		  }
	  
	  private void writeMetaData(ResultSet resultSet) throws SQLException {
		    // Now get some metadata from the database
		    // Result set get the result of the SQL query
		    System.out.println("The columns in the table are: ");
		    System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
		    for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
		      System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
		    }
		  }
	  
	  private void writeResultSet(ResultSet resultSet) throws SQLException {
		    while (resultSet.next()) {
		      String project = resultSet.getString("project_id");
		      }
		  }
	  
	  ReturnTripple getInterested() {
			
			String experts[];
			String projects[];
			String interests[];
			
			List<String> expert = new ArrayList<String>();
			List<String> project = new ArrayList<String>();
			List<String> interest = new ArrayList<String>();

			try {
				//resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
				// resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space");
				
				// To randomize and make the data balanced.
				// SELECT COUNT(`eps_id`) FROM `expert_project_space` WHERE `current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied'
				
				statement = connect.createStatement();
				resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space WHERE expert_id IN (SELECT expert_id FROM expert WHERE profile_complete_percent >= 60) AND (`current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied' OR `current_space` = 'dislike')");
				
				while (resultSet.next()) {
					expert.add(String.valueOf(resultSet.getInt(1)));
					project.add(String.valueOf(resultSet.getInt(2)));
					interest.add(resultSet.getString(3));
				}
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
			experts = expert.toArray(new String[expert.size()]);
			projects = project.toArray(new String[project.size()]);
			interests = interest.toArray(new String[interest.size()]);
			
			this.returnTripple = new ReturnTripple(experts, projects, interests);
			return this.returnTripple;
		}
	  
	  ReturnTripple getBalancedInterested() {
			
			String experts[];
			String projects[];
			String interests[];
			
			List<String> expert = new ArrayList<String>();
			List<String> project = new ArrayList<String>();
			List<String> interest = new ArrayList<String>();

			try {
				//resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
				// resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space");
				
				// To randomize and make the data balanced.
				// SELECT COUNT(`eps_id`) FROM `expert_project_space` WHERE `current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied'
				
				statement = connect.createStatement();
				resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space WHERE `current_space` = 'liked' OR `current_space` = 'saved' OR `current_space` = 'applied'");
				
				int count = 0;
				while (resultSet.next()) {
					expert.add(String.valueOf(resultSet.getInt(1)));
					project.add(String.valueOf(resultSet.getInt(2)));
					interest.add(resultSet.getString(3));
					count++;
				}
				System.out.println("Total like records = " + count);
				try {
					statement = connect.createStatement();
					resultSet = statement.executeQuery("SELECT expert_id, project_id, current_space FROM expert_project_space WHERE `current_space` = 'dislike' LIMIT " + count);
					while (resultSet.next()) {
						expert.add(String.valueOf(resultSet.getInt(1)));
						project.add(String.valueOf(resultSet.getInt(2)));
						interest.add(resultSet.getString(3));
						}
				} 
				 catch (SQLException e) {
					e.printStackTrace();
				}
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
			experts = expert.toArray(new String[expert.size()]);
			projects = project.toArray(new String[project.size()]);
			interests = interest.toArray(new String[interest.size()]);
			
			this.returnTripple = new ReturnTripple(experts, projects, interests);
			return this.returnTripple;
		}
	  
	  public String[] getUniqueProjects() {
			
			String projects[];
			List<String> project = new ArrayList<String>();
			
			try {
				statement = connect.createStatement();
				//resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
				resultSet = statement.executeQuery("SELECT DISTINCT project_id FROM `expert_project_space` WHERE project_id > 40000 AND project_id < 45001");
				while (resultSet.next()) {
					project.add(String.valueOf(resultSet.getInt(1)));
				}
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
			projects = project.toArray(new String[project.size()]);
			return projects;
		}
	  
	  public String[] getUniqueExperts() {
			
			String experts[];
			List<String> expert = new ArrayList<String>();
			
			try {
				statement = connect.createStatement();
				//resultSet = statement.executeQuery("SELECT DISTINCT expert_id FROM `expert_project_space` WHERE `expert_id` > 4000");
				resultSet = statement.executeQuery("SELECT DISTINCT expert_id FROM `expert_project_space` WHERE `expert_id` IN (SELECT expert_id FROM expert WHERE profile_complete_percent >= 60)");
				
				while (resultSet.next()) {
					expert.add(String.valueOf(resultSet.getInt(1)));
				}
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
			experts = expert.toArray(new String[expert.size()]);
			return experts;
		}
	  
	  public void savePersonalities() throws IOException {
			
			List<String> personality = new ArrayList<String>();
			
			try {
				statement = connect.createStatement();
				//resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
				resultSet = statement.executeQuery("SELECT p_title FROM personality");
				
				File fout = new File("Personality.txt");
				FileOutputStream fos = new FileOutputStream(fout);
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			 
				bw.write("<root>");
				bw.newLine();
				
			 	while (resultSet.next()) {
					personality.add(resultSet.getString(1));
					String keyStr = "<s> <ENAMEX TYPE="+ '"' + "PERSONALITY" + '"' + ">" + (resultSet.getString(1)).trim() + "</ENAMEX> </s>";
					bw.write(keyStr);
					bw.newLine();
				}
				bw.write("</root>");
				bw.newLine();
				bw.close();
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
		}
	  
	  public void saveKnowledge() throws IOException {
			
			List<String> knowledge = new ArrayList<String>();
			
			try {
				statement = connect.createStatement();
				//resultSet = statement.executeQuery("SELECT expert_id, project_id, interested FROM project_interests WHERE action = " + "'None'");
				resultSet = statement.executeQuery("SELECT know_title FROM knowledge");
				
				File fout = new File("Knowledge.txt");
				FileOutputStream fos = new FileOutputStream(fout);
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			 
				bw.write("<root>");
				bw.newLine();
				
			 	while (resultSet.next()) {
					knowledge.add(resultSet.getString(1));
					String keyStr = "<s> <ENAMEX TYPE="+ '"' + "KNOWLEDGE" + '"' + ">" + (resultSet.getString(1)).trim() + "</ENAMEX> </s>";
					bw.write(keyStr);
					bw.newLine();
				}
				bw.write("</root>");
				bw.newLine();
				bw.close();
			} 
			 catch (SQLException e) {
				e.printStackTrace();
			}
		}
	  
	// You need to close the resultSet
	  public void close() {
	    try {
	      if (resultSet != null) {
	        resultSet.close();
	      }

	      if (statement != null) {
	        statement.close();
	      }

	      if (connect != null) {
	        connect.close();
	      }
	    } catch (Exception e) {

	    }
	  }
}
