package dbConnection;

public class ReturnPair {

	String jobTitles[];
	String jobSummary = "";
	
	public ReturnPair(String titles[], String summary) {
		this.jobTitles = titles;
		this.jobSummary = summary;
	}
}
