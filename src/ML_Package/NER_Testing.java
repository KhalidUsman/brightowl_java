package ML_Package;

//import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import java.io.FileReader;
//import java.util.ArrayList;
import java.util.Set;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunker;
import com.aliasi.chunk.Chunking;
import com.aliasi.util.AbstractExternalizable;

public class NER_Testing {
	
	private Chunker chunker = null;

	public NER_Testing() throws ClassNotFoundException, IOException {
		File modelFile = new File("C:\\Users\\Xorlogics-ML\\Desktop\\TrainingData\\EducationSkillsLocationFuncPersonKnow.model");
        chunker = (Chunker) AbstractExternalizable.readObject(modelFile);
	}
	
	public Map<String,String> getCategories(String[] crawlFeatures) {

		final Map<String, String> categories = new HashMap<String, String>();
		for (final String feature : crawlFeatures) {
			Chunking chunking = chunker.chunk(feature);
            Set<Chunk> test = chunking.chunkSet();
            for (Chunk c : test) {
            	//System.out.println("Test String is = " + feature.substring(c.start(), c.end()) + "Category is = " + c.type());
            	categories.put(feature.substring(c.start(), c.end()), c.type());
            }
		}
		return categories;
	}
	
	/*
	public static void main(String[] args) throws Exception {
        File modelFile = new File("C:\\Users\\Xorlogics-ML\\Desktop\\TrainingData\\EducationSkillsLocationFunc.model");
        Chunker chunker = (Chunker) AbstractExternalizable
                .readObject(modelFile);
        String testString="competencies";
            Chunking chunking = chunker.chunk(testString);
            Set<Chunk> test = chunking.chunkSet();
            System.out.println(test);
            for (Chunk c : test) {
                System.out.println(testString + " : "
                        + testString.substring(c.start(), c.end()) + " >> "
                        + c.type());

        }
    }
    */
}
