package main;

import dbConnection.Connect_Database;
import dbConnection.Expert;
import dbConnection.Project;
import main.Main.values;
import Keyword_Extraction.Rake;
import ML_Package.NER_Testing;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;


import javax.tools.DocumentationTool.Location;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.DatabaseMetaData;
import com.mysql.jdbc.StringUtils;

public class Main {

	public static double cosine_Score = 0.0;
	
	public static ArrayList<String> listOfPermutation = new ArrayList<String>();
	
	private static Object self;
	
	static <T> T[] joinArrayGeneric(T[]... arrays) {
        int length = 0;
        for (T[] array : arrays) {
            length += array.length;
        }

        //T[] result = new T[length];
        final T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), length);

        int offset = 0;
        for (T[] array : arrays) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }
	
	public static void main(String[] args) throws Exception {
	    
		Connect_Database dao = new Connect_Database();
	    dao.readDataBase();
	    
	    ResultSet expertsForExperts = dao.getExperts();
	    ResultSet projectsForExperts = dao.getProjectsWithId();
	    ResultSet projectsForProjects = dao.getProjects();
	    ResultSet expertsForProjects = dao.getExpertsWithId();
	    
	    //Save into Arrays
	    List<Integer> listExpForExperts = new ArrayList<Integer>();
	    List<Integer> listProForExperts = new ArrayList<Integer>();
	    while (expertsForExperts.next()) {
	    	listExpForExperts.add(expertsForExperts.getInt("expert_Id"));
	    }
	    while (projectsForExperts.next()) {
	    	listProForExperts.add(projectsForExperts.getInt("project_id"));
	    	System.out.println("Project id = " + projectsForExperts.getInt("project_id"));
	    }
	    List<Integer> listProForProjects = new ArrayList<Integer>();
	    List<Integer> listExpForProjects = new ArrayList<Integer>();
	    while (projectsForProjects.next()) {
	    	listProForProjects.add(projectsForProjects.getInt("project_id"));
	    }
	    while (expertsForProjects.next()) {
	    	listExpForProjects.add(expertsForProjects.getInt("expert_Id"));
	    	System.out.println("Expert id = " + expertsForProjects.getInt("expert_Id"));
	    }
	    dao.close();
	    Main.calculateMatchingexpertsForProject(listExpForExperts, listProForExperts);
	    Main.calculateMatchingprojectsForExpert(listExpForProjects, listProForProjects);
	  }
	    
public static void calculateMatchingprojectsForExpert(List<Integer> experts, List<Integer> projects) throws Exception {
			
		ArrayList<Map<String, Double>> matchingArr = new ArrayList<Map<String, Double>>();
		for (int i=0; i<experts.size(); i++) {
			
			Expert expertObj = new Expert(experts.get(i));
			
			String expertPersonalities[] = expertObj.getExpertPersonalities();
		    String expertKnowledge[] = expertObj.getExpertKnowledge();
		    String expertExpertise[] = expertObj.getExpertExpertise();
		    String expertEducation[] = expertObj.getExpertEducation();
		    String expertLanguages[] = expertObj.getExpertLanguages();
		    String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
		    String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
		    String[] combineFuncTitles = (String[])ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
		    String expertLocation[] = expertObj.getExpertLocation();
		    String expertCoordinates[] = expertObj.getExpertCoordinates();
		      
		    String[] combineExpert = joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation);
		    String combineExpertStr = Arrays.toString(combineExpert).replace("amp;","").replace("[","").replace("]","").replace("&"," and ");
		    
		    expertObj.close();
		      
		    Main csObj = new Main();
              
            Hashtable<String, values> word_freq_vector = new Hashtable<String, Main.values>();
      		LinkedList<String> Distinct_words_text_1_2 = new LinkedList<String>();
      		  
      		ReturnVector vecExpSkill = csObj.getComVector(expertExpertise, word_freq_vector, Distinct_words_text_1_2, 0.1, 1);
            ReturnVector vecExpFunc = csObj.getComVector(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, 0.1, 1);
            ReturnVector vecExpLoc = csObj.getComVector(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, 0.1, 1);
            ReturnVector vecExpEdu = csObj.getComVector(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, 0.1, 1);
            ReturnVector vecExpLang = csObj.getComVector(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, 2.0, 1);
            ReturnVector vecExpPer = csObj.getComVector(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, 2.0, 1);
            ReturnVector vecExpKnow = csObj.getComVector(expertKnowledge, vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, 1.3, 1);
		    
            for (int j=0; j<projects.size();j++) {
            	Project projectObj = new Project(projects.get(j));
            	if (projectObj._crawlId == 0) {
                	
		    		String projectPersonlities[] = projectObj.getProjectPersonalities();
		            String projectKnowledge[] = projectObj.getProjectKnowledge();
		            String projectExperties[] = projectObj.getProjectExpertise();
		            String projectEducation[] = projectObj.getProjectEducation();
		            String projectLanguages[] = projectObj.getProjectLanguages();
		            String projectFuncTitles[] = projectObj.getProjectFunctionalTitle();
		            String projectLocation[] = projectObj.getProjectLocation();
		            String projectCoordinates[] = projectObj.getProjectCoordinates();
		            
		            String[] combineProject = joinArrayGeneric(projectPersonlities, projectKnowledge, projectExperties, projectEducation, projectLanguages, projectFuncTitles, projectLocation);
		            String combineProjectStr = Arrays.toString(combineProject).replace("amp;","").replace("[","").replace("]","").replace("&"," and ");
		            
		            double featureScore = csObj.Cosine_Similarity_Score(combineExpertStr, combineProjectStr);
		            
		            ReturnVector vecProSkill = csObj.getComVector(projectExperties, vecExpKnow.word_freq_vector, vecExpKnow.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProFunc = csObj.getComVector(projectFuncTitles, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProLoc = csObj.getComVector(projectLocation, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProEdu = csObj.getComVector(projectEducation, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProLang = csObj.getComVector(projectLanguages, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0);
		      		ReturnVector vecProPer = csObj.getComVector(projectPersonlities, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0);
		      		ReturnVector vecProKnow = csObj.getComVector(projectKnowledge, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0);
		      		
		      		double avgDistance = Main.distance(expertCoordinates, projectCoordinates);
		      		
		      		cosine_Score = csObj.cosine_Score(vecProKnow);
		            
		      		double avgScore = 0.6*(cosine_Score) + 0.2*(featureScore) + 0.2*((500 - avgDistance)/500);
		      		
		            final Map<String, Double> matchingDic = new HashMap<String, Double>();
				    matchingDic.put("expert_id", (double) expertObj._expertId);
		            matchingDic.put("project_id", (double) projectObj._projectId);
		            matchingDic.put("ml_score", avgScore);
		            matchingArr.add(matchingDic);
		            
		            //System.out.println("Expert = " + experts.get(i) + " Project = " + projects.get(j) + " Cosine = " + featureScore + " Category Cosine = " + cosine_Score + " Avg Score = " + avgScore);
                }
                else {
                	String crawlCoordinates[] = projectObj.getProjectCoordinates();
		    		double avgDistance = Main.distance(expertCoordinates, crawlCoordinates);
		    		  
		    		String crawlString = (projectObj._crawlText).replaceAll("\\<[^>]*>"," ").replace("nbsp;","").replace("amp;","").replace("&"," and ");
		    		Rake rakeObj = new Rake();
		    		String crawlFeatures[] = rakeObj.run(crawlString);
	    			  
		    		// LingPipe Categories
		    		NER_Testing testObj = new NER_Testing();
		    		final Map<String, String> categories = testObj.getCategories(crawlFeatures);
		    		  
		    		List<String> crawlSkills = new ArrayList<String>();
		    		List<String> crawlFunctionTitle = new ArrayList<String>();
		    		List<String> crawlLocation = new ArrayList<String>();
		    		List<String> crawlEducation = new ArrayList<String>();
		    		List<String> crawlLanguage = new ArrayList<String>();
		    		List<String> crawlPersonality = new ArrayList<String>();
		    		List<String> crawlKnowledge = new ArrayList<String>();
		    		  
		    		for (Map.Entry<String, String> entry : categories.entrySet()) {
		    			if(entry.getValue().equals("SKILL") == true) {
		    				crawlSkills.add(entry.getKey());
		    				}
		    			else if(entry.getValue().equals("FUNCTIONTITLE") == true) {
		    				crawlFunctionTitle.add(entry.getKey());
		    				}
		    			else if(entry.getValue().equals("LOCATION") == true) {
		    				crawlLocation.add(entry.getKey());
		    				}
		    			else if((entry.getValue().equals("EDUCATION")) == true || (entry.getValue().equals("DEGREE") == true)) {
		    				crawlEducation.add(entry.getKey());
		    				}
		    			else if(entry.getValue().equals("LANGUAGE") == true) {
		    				crawlLanguage.add(entry.getKey());
		    				}
		    			else if(entry.getValue().equals("PERSONALITY") == true) {
		    				crawlPersonality.add(entry.getKey());
		    				}
		    			else if(entry.getValue().equals("KNOWLEDGE") == true) {
		    				crawlKnowledge.add(entry.getKey());
		    				}
		    			  else {
		    				  //System.out.println("Useless Category");
		    				  }
		    			  }
		    		  
		    		String crawlSkillArr[] = crawlSkills.toArray(new String[crawlSkills.size()]);
		    		String crawlFunctionTitleArr[] = crawlFunctionTitle.toArray(new String[crawlFunctionTitle.size()]);
		    		String crawlLocationArr[] = crawlLocation.toArray(new String[crawlLocation.size()]);
		    		String crawlEducationArr[] = crawlEducation.toArray(new String[crawlEducation.size()]);
		    		String crawlLanguageArr[] = crawlLanguage.toArray(new String[crawlLanguage.size()]);
		    		String crawlPersonalityArr[] = crawlPersonality.toArray(new String[crawlPersonality.size()]);
		    		String crawlKnowledgeArr[] = crawlKnowledge.toArray(new String[crawlKnowledge.size()]);
		    		  
		    		ReturnVector vecProSkill = csObj.getComVector(crawlSkillArr, vecExpKnow.word_freq_vector, vecExpKnow.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProFunc = csObj.getComVector(crawlFunctionTitleArr, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProLoc = csObj.getComVector(crawlLocationArr, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProEdu = csObj.getComVector(crawlEducationArr, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0);
		      		ReturnVector vecProLang = csObj.getComVector(crawlLanguageArr, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0);
		      		ReturnVector vecProPer = csObj.getComVector(crawlPersonalityArr, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0);
		      		ReturnVector vecProKnow = csObj.getComVector(crawlKnowledgeArr, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0);
		      		  
		      		cosine_Score = csObj.cosine_Score(vecProKnow);
		      		
		      		String crawlFeaturesStr = Arrays.toString(crawlFeatures).replace("amp;","").replace("[","").replace("]","");
		    		double featureScore = csObj.Cosine_Similarity_Score(combineExpertStr, crawlFeaturesStr);
		    		double avgScore = 0.6*(cosine_Score) + 0.2*(featureScore) + 0.2*((500 - avgDistance)/500);
		    		  
		    		final Map<String, Double> matchingDic = new HashMap<String, Double>();
		  		    matchingDic.put("expert_id", (double) expertObj._expertId);
		    		matchingDic.put("project_id", (double) projectObj._projectId);
		            matchingDic.put("ml_score", avgScore);
		            matchingArr.add(matchingDic);
		              
		            //System.out.println(" Avg Score = " + avgScore  + " Cosine Score = " + cosine_Score + " Features Score = " + featureScore);
                }
                projectObj.close();
            }
		}
		// Check if Table exists
		//String sql = "INSERT OR REPLACE INTO expert_project_complete (expert_id, project_id, match_ml, ml_score_kh) " + "VALUES (?, ?, ?, ?)";
		Connect_Database saveRec = new Connect_Database();
		saveRec.readDataBase();
	    try {
	    	//saveRec.saveRecords(matchingArr, sql);
	    	saveRec.InsertOrUpdateRecord(matchingArr);
	    	for(int k=0;k<projects.size();k++) {
		    	String sql = "UPDATE projects SET k_ml_update = 0 WHERE project_id = " + projects.get(k);
			    try {
			    	saveRec.updateRecords(sql);
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
	    	for(int k=0;k<experts.size();k++) {
		    	String sql = "UPDATE expert SET k_ml_update = 0 WHERE expert_id = " + experts.get(k);
			    try {
			    	saveRec.updateRecords(sql);
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
	    	System.out.println("Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	    finally {
	    	saveRec.close();
	    }
	}

public static void calculateMatchingexpertsForProject(List<Integer> experts, List<Integer> projects) throws Exception {
	
	ArrayList<Map<String, Double>> matchingArr = new ArrayList<Map<String, Double>>();
	
	Main csObj = new Main();
    Hashtable<String, values> word_freq_vector = new Hashtable<String, Main.values>();
	LinkedList<String> Distinct_words_text_1_2 = new LinkedList<String>();
	
	for (int j=0; j<projects.size();j++) {
    	Project projectObj = new Project(projects.get(j));
    	if (projectObj._crawlId == 0) {
        	
    		String projectPersonlities[] = projectObj.getProjectPersonalities();
            String projectKnowledge[] = projectObj.getProjectKnowledge();
            String projectExperties[] = projectObj.getProjectExpertise();
            String projectEducation[] = projectObj.getProjectEducation();
            String projectLanguages[] = projectObj.getProjectLanguages();
            String projectFuncTitles[] = projectObj.getProjectFunctionalTitle();
            String projectLocation[] = projectObj.getProjectLocation();
            String projectCoordinates[] = projectObj.getProjectCoordinates();
            
            String[] combineProject = joinArrayGeneric(projectPersonlities, projectKnowledge, projectExperties, projectEducation, projectLanguages, projectFuncTitles, projectLocation);
            String combineProjectStr = Arrays.toString(combineProject).replace("amp;","").replace("[","").replace("]","").replace("&"," and ");
            
            ReturnVector vecProSkill = csObj.getComVector(projectExperties, word_freq_vector, Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProFunc = csObj.getComVector(projectFuncTitles, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProLoc = csObj.getComVector(projectLocation, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProEdu = csObj.getComVector(projectEducation, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProLang = csObj.getComVector(projectLanguages, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0);
      		ReturnVector vecProPer = csObj.getComVector(projectPersonlities, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0);
      		ReturnVector vecProKnow = csObj.getComVector(projectKnowledge, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0);
      		
      		for (int i=0; i<experts.size(); i++) {
      			Expert expertObj = new Expert(experts.get(i));
      			String expertPersonalities[] = expertObj.getExpertPersonalities();
      		    String expertKnowledge[] = expertObj.getExpertKnowledge();
      		    String expertExpertise[] = expertObj.getExpertExpertise();
      		    String expertEducation[] = expertObj.getExpertEducation();
      		    String expertLanguages[] = expertObj.getExpertLanguages();
      		    String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
      		    String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
      		    String[] combineFuncTitles = (String[])ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
      		    String expertLocation[] = expertObj.getExpertLocation();
      		    String expertCoordinates[] = expertObj.getExpertCoordinates();
      		      
      		    String[] combineExpert = joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation);
      		    String combineExpertStr = Arrays.toString(combineExpert).replace("amp;","").replace("[","").replace("]","").replace("&"," and ");
      		    
      		    expertObj.close();
      		    
      		   double featureScore = csObj.Cosine_Similarity_Score(combineExpertStr, combineProjectStr);
      		    
      	  		ReturnVector vecExpSkill = csObj.getComVector(expertExpertise, vecProKnow.word_freq_vector, vecProKnow.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpFunc = csObj.getComVector(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpLoc = csObj.getComVector(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpEdu = csObj.getComVector(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpLang = csObj.getComVector(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, 2.0, 1);
      	        ReturnVector vecExpPer = csObj.getComVector(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, 2.0, 1);
      	        ReturnVector vecExpKnow = csObj.getComVector(expertKnowledge, vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, 1.3, 1);
      	        
      	        cosine_Score = csObj.cosine_Score(vecProKnow);
      	        double avgDistance = Main.distance(expertCoordinates, projectCoordinates);
      	        double avgScore = 0.6*(cosine_Score) + 0.2*(featureScore) + 0.2*((500 - avgDistance)/500);
      		
      	        final Map<String, Double> matchingDic = new HashMap<String, Double>();
      	        matchingDic.put("expert_id", (double) expertObj._expertId);
      	        matchingDic.put("project_id", (double) projectObj._projectId);
      	        matchingDic.put("ml_score", avgScore);
      	        matchingArr.add(matchingDic);
      		}
            //System.out.println("Expert = " + experts.get(i) + " Project = " + projects.get(j) + " Cosine = " + featureScore + " Category Cosine = " + cosine_Score + " Avg Score = " + avgScore);
        }
        else {
        	String crawlString = (projectObj._crawlText).replaceAll("\\<[^>]*>"," ").replace("nbsp;","").replace("amp;","").replace("&"," and ");
    		Rake rakeObj = new Rake();
    		String crawlFeatures[] = rakeObj.run(crawlString);
			  
    		// LingPipe Categories
    		NER_Testing testObj = new NER_Testing();
    		final Map<String, String> categories = testObj.getCategories(crawlFeatures);
    		  
    		List<String> crawlSkills = new ArrayList<String>();
    		List<String> crawlFunctionTitle = new ArrayList<String>();
    		List<String> crawlLocation = new ArrayList<String>();
    		List<String> crawlEducation = new ArrayList<String>();
    		List<String> crawlLanguage = new ArrayList<String>();
    		List<String> crawlPersonality = new ArrayList<String>();
    		List<String> crawlKnowledge = new ArrayList<String>();
    		  
    		for (Map.Entry<String, String> entry : categories.entrySet()) {
    			if(entry.getValue().equals("SKILL") == true) {
    				crawlSkills.add(entry.getKey());
    				}
    			else if(entry.getValue().equals("FUNCTIONTITLE") == true) {
    				crawlFunctionTitle.add(entry.getKey());
    				}
    			else if(entry.getValue().equals("LOCATION") == true) {
    				crawlLocation.add(entry.getKey());
    				}
    			else if((entry.getValue().equals("EDUCATION")) == true || (entry.getValue().equals("DEGREE") == true)) {
    				crawlEducation.add(entry.getKey());
    				}
    			else if(entry.getValue().equals("LANGUAGE") == true) {
    				crawlLanguage.add(entry.getKey());
    				}
    			else if(entry.getValue().equals("PERSONALITY") == true) {
    				crawlPersonality.add(entry.getKey());
    				}
    			else if(entry.getValue().equals("KNOWLEDGE") == true) {
    				crawlKnowledge.add(entry.getKey());
    				}
    			  else {
    				  //System.out.println("Useless Category");
    				  }
    			  }
    		  
    		String crawlSkillArr[] = crawlSkills.toArray(new String[crawlSkills.size()]);
    		String crawlFunctionTitleArr[] = crawlFunctionTitle.toArray(new String[crawlFunctionTitle.size()]);
    		String crawlLocationArr[] = crawlLocation.toArray(new String[crawlLocation.size()]);
    		String crawlEducationArr[] = crawlEducation.toArray(new String[crawlEducation.size()]);
    		String crawlLanguageArr[] = crawlLanguage.toArray(new String[crawlLanguage.size()]);
    		String crawlPersonalityArr[] = crawlPersonality.toArray(new String[crawlPersonality.size()]);
    		String crawlKnowledgeArr[] = crawlKnowledge.toArray(new String[crawlKnowledge.size()]);
    		  
    		ReturnVector vecProSkill = csObj.getComVector(crawlSkillArr, word_freq_vector, Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProFunc = csObj.getComVector(crawlFunctionTitleArr, vecProSkill.word_freq_vector, vecProSkill.Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProLoc = csObj.getComVector(crawlLocationArr, vecProFunc.word_freq_vector, vecProFunc.Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProEdu = csObj.getComVector(crawlEducationArr, vecProLoc.word_freq_vector, vecProLoc.Distinct_words_text_1_2, 0.1, 0);
      		ReturnVector vecProLang = csObj.getComVector(crawlLanguageArr, vecProEdu.word_freq_vector, vecProEdu.Distinct_words_text_1_2, 2.0, 0);
      		ReturnVector vecProPer = csObj.getComVector(crawlPersonalityArr, vecProLang.word_freq_vector, vecProLang.Distinct_words_text_1_2, 2.0, 0);
      		ReturnVector vecProKnow = csObj.getComVector(crawlKnowledgeArr, vecProPer.word_freq_vector, vecProPer.Distinct_words_text_1_2, 1.3, 0);
	
      		for (int i=0; i<experts.size(); i++) {
      			Expert expertObj = new Expert(experts.get(i));
      			String expertPersonalities[] = expertObj.getExpertPersonalities();
      		    String expertKnowledge[] = expertObj.getExpertKnowledge();
      		    String expertExpertise[] = expertObj.getExpertExpertise();
      		    String expertEducation[] = expertObj.getExpertEducation();
      		    String expertLanguages[] = expertObj.getExpertLanguages();
      		    String expertFuncTitles[] = expertObj.getExpertFunctionalTitle();
      		    String expertFuncTitlesInterested[] = expertObj.getExpertFuncTitleInterested();
      		    String[] combineFuncTitles = (String[])ArrayUtils.addAll(expertFuncTitles, expertFuncTitlesInterested);
      		    String expertLocation[] = expertObj.getExpertLocation();
      		    String expertCoordinates[] = expertObj.getExpertCoordinates();
      		      
      		    String[] combineExpert = joinArrayGeneric(expertPersonalities, expertKnowledge, expertExpertise, expertEducation, expertLanguages, combineFuncTitles, expertLocation);
      		    String combineExpertStr = Arrays.toString(combineExpert).replace("amp;","").replace("[","").replace("]","").replace("&"," and ");
      		    
      		    expertObj.close();
      		      
      	  		ReturnVector vecExpSkill = csObj.getComVector(expertExpertise, vecProKnow.word_freq_vector, vecProKnow.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpFunc = csObj.getComVector(combineFuncTitles, vecExpSkill.word_freq_vector, vecExpSkill.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpLoc = csObj.getComVector(expertLocation, vecExpFunc.word_freq_vector, vecExpFunc.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpEdu = csObj.getComVector(expertEducation, vecExpLoc.word_freq_vector, vecExpLoc.Distinct_words_text_1_2, 0.1, 1);
      	        ReturnVector vecExpLang = csObj.getComVector(expertLanguages, vecExpEdu.word_freq_vector, vecExpEdu.Distinct_words_text_1_2, 2.0, 1);
      	        ReturnVector vecExpPer = csObj.getComVector(expertPersonalities, vecExpLang.word_freq_vector, vecExpLang.Distinct_words_text_1_2, 2.0, 1);
      	        ReturnVector vecExpKnow = csObj.getComVector(expertKnowledge, vecExpPer.word_freq_vector, vecExpPer.Distinct_words_text_1_2, 1.3, 1);
	      		
      	        String crawlCoordinates[] = projectObj.getProjectCoordinates();
      	        double avgDistance = Main.distance(expertCoordinates, crawlCoordinates);
	      		cosine_Score = csObj.cosine_Score(vecProKnow);
	      		
	      		String crawlFeaturesStr = Arrays.toString(crawlFeatures).replace("amp;","").replace("[","").replace("]","");
	    		double featureScore = csObj.Cosine_Similarity_Score(combineExpertStr, crawlFeaturesStr);
	    		double avgScore = 0.6*(cosine_Score) + 0.2*(featureScore) + 0.2*((500 - avgDistance)/500);
	    		  
	    		final Map<String, Double> matchingDic = new HashMap<String, Double>();
	  		    matchingDic.put("expert_id", (double) expertObj._expertId);
	    		matchingDic.put("project_id", (double) projectObj._projectId);
	            matchingDic.put("ml_score", avgScore);
	            matchingArr.add(matchingDic);
	              
	            //System.out.println(" Avg Score = " + avgScore  + " Cosine Score = " + cosine_Score + " Features Score = " + featureScore);
	            }
            }
            projectObj.close();
	}
	// Check if Table exists
	//String sql = "INSERT OR REPLACE INTO expert_project_complete (expert_id, project_id, match_ml, ml_score_kh) " + "VALUES (?, ?, ?, ?)";
	Connect_Database saveRec = new Connect_Database();
	saveRec.readDataBase();
    try {
    	//saveRec.saveRecords(matchingArr, sql);
    	saveRec.InsertOrUpdateRecord(matchingArr);
    	for(int k=0;k<projects.size();k++) {
	    	String sql = "UPDATE projects SET k_ml_update = 0 WHERE project_id = " + projects.get(k);
		    try {
		    	saveRec.updateRecords(sql);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
    	for(int k=0;k<experts.size();k++) {
	    	String sql = "UPDATE expert SET k_ml_update = 0 WHERE expert_id = " + experts.get(k);
		    try {
		    	saveRec.updateRecords(sql);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
    	System.out.println("Done");
	} catch (Exception e) {
		e.printStackTrace();
	}
    finally {
    	saveRec.close();
    }
}

	private static int Int(Integer integer) {
	return 0;
}
	
	// Calculate Cosine Similarity between two texts
	public class values {
		double val1;
		double val2;
	
	values(double v1, double v2) {
		this.val1=v1;
		this.val2=v2;
		}
	
	public void Update_VAl(double v1, double v2) {
		this.val1=v1;
		this.val2=v2;
		}
	}
	//end of class values
	
	// Vector Class
	public class ReturnVector {
		
		Hashtable<String, values> word_freq_vector = new Hashtable<String, Main.values>();
		LinkedList<String> Distinct_words_text_1_2 = new LinkedList<String>();
		
		public ReturnVector(Hashtable<String, values> word_freq_text, LinkedList<String> Distinct_words) {
			this.word_freq_vector = word_freq_text;
			this.Distinct_words_text_1_2 = Distinct_words;
		}
	}
	
	public ReturnVector getComVector(String text[], Hashtable<String, values> word_freq_text, LinkedList<String> Distinct_words, double weight, int valueId) {
	   
		//prepare word frequency vector by using Text1
		for(int i=0;i<text.length;i++) {
			String tmp_wd = text[i].trim();
			if(tmp_wd.length()>0) {
				if(word_freq_text.containsKey(tmp_wd)) {
					values vals1 = word_freq_text.get(tmp_wd);
					// ValueId = 1 for Expert and else for Project
					if (valueId == 1) {
						double freq1 = vals1.val1+1;
						double freq2 = vals1.val2;
						vals1.Update_VAl(freq1, freq2);
					}
					else {
						double freq1 = vals1.val1;
						double freq2 = vals1.val2+1;
						vals1.Update_VAl(freq1, freq2);
					}
					word_freq_text.put(tmp_wd, vals1);
					}
				else {
					if (valueId == 1) {
						values vals1 = new values(1, 0);
						word_freq_text.put(tmp_wd, vals1);
					}
					else {
						values vals1 = new values(0, 1);
						word_freq_text.put(tmp_wd, vals1);
					}
					Distinct_words.add(tmp_wd);
					}
				}
			}
		for(int i=0;i<Distinct_words.size();i++) {
      	  String keyword = Distinct_words.get(i);
      	  values vals1 = word_freq_text.get(keyword);
      	  if (valueId == 1) {
      		  double freq1 = vals1.val1*weight;
      		  double freq2 = vals1.val2;
        	  vals1.Update_VAl(freq1, freq2);  
      	  }
      	  else {
      		  double freq1 = vals1.val1;
      		  double freq2 = vals1.val2*weight;
        	  vals1.Update_VAl(freq1, freq2);
      	  }
      	word_freq_text.put(keyword, vals1);
        }
		
		ReturnVector retVector = new ReturnVector(word_freq_text, Distinct_words);
		return retVector;
	}
	
	public double cosine_Score (ReturnVector rv) {
		double sim_score=0.0000000;
		//calculate the cosine similarity score.
		double VectAB = 0.0000000;
		double VectA_Sq = 0.0000000;
		double VectB_Sq = 0.0000000;
			   
		for(int i=0;i<rv.Distinct_words_text_1_2.size();i++) {
			values vals12 = rv.word_freq_vector.get(rv.Distinct_words_text_1_2.get(i));
			double freq1 = (double)vals12.val1;
			double freq2 = (double)vals12.val2;
			//System.out.println(rv.Distinct_words_text_1_2.get(i)+"#"+freq1+"#"+freq2);
					
			VectAB=VectAB+(freq1*freq2);
			VectA_Sq = VectA_Sq + freq1*freq1;
			VectB_Sq = VectB_Sq + freq2*freq2;
			}
			//System.out.println("VectAB "+VectAB+" VectA_Sq "+VectA_Sq+" VectB_Sq "+VectB_Sq);
			sim_score = ((VectAB)/(Math.sqrt(VectA_Sq)*Math.sqrt(VectB_Sq)));
			   
			return(sim_score);
	}
	
	public double Cosine_Similarity_Score(String Text1, String Text2) {
	  
		double sim_score=0.0000000;
		//1. Identify distinct words from both documents
		String [] word_seq_text1 = Text1.split(" ");
		String [] word_seq_text2 = Text2.split(" ");
		Hashtable<String, values> word_freq_vector = new Hashtable<String, Main.values>();
		LinkedList<String> Distinct_words_text_1_2 = new LinkedList<String>();
	   
		//prepare word frequency vector by using Text1
		for(int i=0;i<word_seq_text1.length;i++) {
			String tmp_wd = word_seq_text1[i].trim();
			if(tmp_wd.length()>0) {
				if(word_freq_vector.containsKey(tmp_wd)) {
					values vals1 = word_freq_vector.get(tmp_wd);
					double freq1 = vals1.val1+1;
					double freq2 = vals1.val2;
					vals1.Update_VAl(freq1, freq2);
					word_freq_vector.put(tmp_wd, vals1);
					}
				else {
					values vals1 = new values(1, 0);
					word_freq_vector.put(tmp_wd, vals1);
					Distinct_words_text_1_2.add(tmp_wd);
					}
				}
			}
		
		//prepare word frequency vector by using Text2
		for(int i=0;i<word_seq_text2.length;i++) {
			String tmp_wd = word_seq_text2[i].trim();
			if(tmp_wd.length()>0) {
				if(word_freq_vector.containsKey(tmp_wd)) {
					values vals1 = word_freq_vector.get(tmp_wd);
					double freq1 = vals1.val1;
					double freq2 = vals1.val2+1;
					vals1.Update_VAl(freq1, freq2);
					word_freq_vector.put(tmp_wd, vals1);
					}
				else {
					values vals1 = new values(0, 1);
					word_freq_vector.put(tmp_wd, vals1);
					Distinct_words_text_1_2.add(tmp_wd);
					}
				}
			}
		
		//calculate the cosine similarity score.
		double VectAB = 0.0000000;
		double VectA_Sq = 0.0000000;
		double VectB_Sq = 0.0000000;
	   
		for(int i=0;i<Distinct_words_text_1_2.size();i++) {
			values vals12 = word_freq_vector.get(Distinct_words_text_1_2.get(i));
			double freq1 = (double)vals12.val1;
			double freq2 = (double)vals12.val2;
			//System.out.println(Distinct_words_text_1_2.get(i)+"#"+freq1+"#"+freq2);
			
			VectAB=VectAB+(freq1*freq2);
			VectA_Sq = VectA_Sq + freq1*freq1;
			VectB_Sq = VectB_Sq + freq2*freq2;
			}
		//System.out.println("VectAB "+VectAB+" VectA_Sq "+VectA_Sq+" VectB_Sq "+VectB_Sq);
		sim_score = ((VectAB)/(Math.sqrt(VectA_Sq)*Math.sqrt(VectB_Sq)));
	   
		return(sim_score);
		}
	
	public static double distance(String []coord1, String []coord2) {

	    final int R = 6371; // Radius of the earth
	    double shortestDistance = 500.0;
	    if (coord1.length>0 && coord2.length>0) {
	    	for (String coord1Str : coord1) {
	    		String[] coord1Arr = coord1Str.split(" ");
	    		if ((!coord1Arr[0].equals("null") && !coord1Arr[0].isEmpty()) && (!coord1Arr[1].equals("null") && !coord1Arr[1].isEmpty())) {
	    		//if (!StringUtils.isNullOrEmpty(coord1Arr[0]) && !StringUtils.isNullOrEmpty(coord1Arr[1])) {
	    			double lat1 = Double.parseDouble(coord1Arr[0]);
	    			double lon1 = Double.parseDouble(coord1Arr[1]);
	    			
	    			for (String coord2Str : coord2) {
	    				String[] coord2Arr = coord2Str.split(" ");
	    				if ((!coord2Arr[0].equals("null") && !coord2Arr[0].isEmpty()) && (!coord2Arr[1].equals("null") && !coord2Arr[1].isEmpty())) {
	    					double lat2 = Double.parseDouble(coord2Arr[0]);
	    	    			double lon2 = Double.parseDouble(coord2Arr[1]);
	    	    			
	    	    		    Double latDistance = Math.toRadians(lat2 - lat1);
	    	    		    Double lonDistance = Math.toRadians(lon2 - lon1);
	    	    		    Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
	    	    		            + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
	    	    		            * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
	    	    		    Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    	    		    double distance = R * c; // convert to meters
	    	    		    if (distance < shortestDistance) {
	    	    		    	shortestDistance = distance;
	    	    		    }
	    	    		    return shortestDistance;
	    				}
	    				else {
	    					return shortestDistance;
	    				}
	    			}
	    		}
	    		else {
	    			return shortestDistance;
	    		}
	    	}
	    }
	    else {
	    	return shortestDistance;
	    }
		return shortestDistance;
	}
}

